# Fluxo de Versionamento

Padronização do fluxo de versionamento dos projetos

## Branches Principais
O repositório central possui três branches principais com vida infinita.
* **dev**
* **master**
 
O branch dev possui todo código já entregue e as últimas de desenvolvimento para a próxima versão. Quando o código do branch dev é considerado estável e pronto para ser implantado, todas as alterações devem ser mescladas de volta para o branch master e criada uma tag. 
 
O branch master é o branch principal, a HEAD do projeto, nele há somente versões que estão em produção.
 
## Branches de suporte
Junto aos branches master e dev utilizamos outros branches de suporte, para correção de erros, criação de melhorias e preparação para implantação. Diferente dos branches principais, esses tem uma vida limitada, uma vez que eles são removidos eventualmente.
 
Os diferentes tipos de branches que usaremos são:
* Branches de melhorias (**feature**)
* Branches de lançamento (**release**)
* Branches de correções (**hotfix**)
 
Cada tipo de branch tem um propósito específico e segue regras de quais branches devem ser originados e mesclados.
 
## Branches de melhorias
Branches de melhorias são usados para desenvolver novas funcionalidades para o próximo lançamento. Em essência, um branch de melhoria existe apenas enquanto está em desenvolvimento, devendo ser mesclado ao branch dev, assumindo que entrará no próximo lançamento, ou descartado caso não seja útil ou seja um experimento.
 
### Regras
Deve ser criado a partir de: **dev**<br>
Deve ser mesclado de volta para: **dev**<br>
Convenção de nome:
* Prefixo: _feature/*_
* Nome: _Qualquer nome exceto master, dev, hmg, release/*, ou hotfix/*_
 
### Criando um branch de melhoria
Ao iniciar o desenvolvimento de uma funcionalidade, crie um branch a partir do branch dev

```
$ git checkout -b feature/xpto dev
Switched to a new branch “feature/xpto”
```
 
### Finalzando um branch de melhoria
Uma vez concluído o desenvolvimento no branch, ele deve ser incorporado de volta no branch dev através de um pull request.
 
Envie seu branch para o servidor:
`$ git push origin feature/xpto`
 
Navegue até a interface do GitLab e clique em New pull request, selecione como base seu branch e o branch dev.<br>
Uma vez aceito o pull request pelo administrador do projeto, a melhoria entrará na próxima versão.
 
## Branches de lançamento
Branches de lançamento são usados para preparação do lançamento da próxima versão de produção. Nele são permitidas pequenas correções e atualização de versão nos arquivos, por exemplo, defines.mk. Fazendo isso no branch de lançamento, o branch dev fica livre para receber novas melhorias para a próxima versão.
 
### Regras
Deve ser criado a partir de: **dev**<br>
Deve ser mesclado de volta para: **dev** ou **master**<br>
Convenção de nome: _release/MAJOR.MINOR.PATCH_
 
Na criação do branch de lançamento é decidido qual versão o projeto terá, até este momento o branch dev reflete as alterações da próxima versão, independente de qual for. Esta decisão é feita na criação do branch de lançamento e segue as convenções de versionamento do projeto.
 
### Criando um branch de lançamento
Branches de lançamento são criados a partir do branch dev. Digamos que o projeto está na versão _1.5.3_ e há uma implantação em breve. O estado do branch dev está pronto para a próxima implantação e decidimos que vai se tornar _1.2.0_ (ao invés de _1.1.5_ ou _2.0.0_). Então criamos um branch com o nome da versão que escolhemos.

```
$ git checkout -b release/1.2.0 dev
Switched to a new branch “release-1.2.0"
```

Depois de criar o branch, a primeira coisa a fazer é aumentar a versão dos arquivos equivalentes e comitar. Podendo ser um _composer.json_, _defines.mk_, um _version.py_, etc. Esta alteração é chamada de bump.

``` 
$ ./bump-version.sh 1.2.0
Files modified successfully, version bumped to 1.2.0
$ git commit -a -m “Bumped version number to 1.2.0”
[release/1.2.0 74d9424] Bumped version number to 1.2.0
1 files changed, 1 insertions(+), 1 deletions(-)
```

Neste exemplo o script _bump-version.sh_ faz estas alterações, mas podem ser feitas manualmente ou utilizando o plugin: <a href="https://www.npmjs.com/package/version-bump-prompt">version-bump-prompt</a>.<br>
Este branch deve existir por enquanto, até que esteja pronto para ser implantado definitivamente em produção. Pequenas correções são permitidas nele (ao invés do branch dev). Adicionar funcionalidades novas nele são proibidas, apenas correções pontuais desta versão de lançamento.
 
### Finalizando um branch de lançamento
Quando o branch de lançamento estiver pronto para ser implantado em produção, algumas ações precisam ser tomadas. Primeiro o branch de lançamento é mesclado com o branch master (uma vez que cada commit no master é uma nova versão, por definição):

``` 
$ git checkout master
Switched to branch ‘master’
$ git merge --no-ff release/1.2.0
Merge made by recursive.
(Summary of changes)
```

Em seguida este commit deve ser tageado para referência futura para esta versão: 
`$ git tag -a 1.2.0`

Finalmente, as mudanças feitas no branch de lançamento precisam mescladas de volta no branch dev, para que as versões futuras também possuam as correções feitas neste branch:

``` 
$ git checkout dev
Switched to branch ‘dev’
$ git merge — no-ff release/1.2.0
Merge made by recursive.
(Summary of changes)
```

Neste momento podem ocorrer alguns conflitos, já que as correções podem mudar a versão dos arquivos, se acontecer, corrija e faça o commit.<br>
Feitos os merges podemos excluir o branch de lançamento, já que não precisamos mais dele:

``` 
$ git branch -d release/1.2.0
Deleted branch release/1.2.0 (was ff452fe).
```

## Branches de correções
Branches de correções são muito parecidos com branches de lançamentos em sua concepção, pois tem o mesmo objetivo de prepara uma versão para produção, embora não planejada. Eles surgem da necessidade de agir imediatamente em uma versão de produção já implantada. Quando um bug crítico ocorre em produção um branch de correção precisa ser criado a partir da tag correspondente.
 
### Regras
Deve ser criado a partir de: **master**<br>
Deve ser mesclado de volta para: **dev** e **master**<br>
Convenção de nome: _hotfix/*_<br>
 
A ideia é que o time que está trabalhando na próxima versão no branch dev possa continuar enquanto alguém prepara uma correção.
 
### Criando um branch de correção
Branches de correção são criados a partir do branch master. Por exemplo, digamos que a versão corrente é a _1.2.0_ e está causando problemas devido a um erro grave em produção. Porém as mudanças no branch dev não estão estáveis ainda. Precisamos criar um branch de correção e começar a corrigir o problema.

``` 
$ git checkout -b hotfix/-1.2.1 master
Switched to a new branch “hotfix/1.2.1”
$ ./bump-version.sh 1.2.1
Files modified successfully, version bumped to 1.2.1.
$ git commit -a -m “Bumped version number to 1.2.1”
[hotfix-1.2.1 41e61bb] Bumped version number to 1.2.1
1 files changed, 1 insertions(+), 1 deletions(-)
```

Não esqueça de aumentar a versão após criar o branch.<br>
Em seguida corrigir o bug e comitar em um ou mais commits.

``` 
$ git commit -m “Fixed sever production problem”
[hotfix-1.2.1 abbe5d6] Fixed sever production problem
5 files changed, 32 insertions(+), 17 deletions(-)
```

### Finalizando um branch de correção
Quando finalizados as correções, o branch deve ser mesclado de volta com o branch master, mas também deve ser mesclado com o branch dev, para que as correções sejam incluídas na próxima versão também. O processo é similar ao do branch de lançamento.
Primeiro atualize o master e crie uma tag.

```
$ git checkout master
Switched to branch ‘master’
$ git merge — no-ff hotfix/1.2.1
Merge made by recursive.
(Summary of changes)
$ git tag -a 1.2.1
```

Em seguida inclua a correção no branch dev.

```
$ git checkout dev
Switched to branch ‘dev’
$ git merge — no-ff hotfix/1.2.1
Merge made by recursive.
(Summary of changes)
```

A única exceção a esse processo é quando existe um branch de lançamento, neste caso as alterações devem ser mescladas nele, ao invés de no branch dev. As alterações mescladas no branch de lançamento refletirão no branch dev também, quando forem finalizadas. Se o branch dev precisar da correção imediatamente e não puder esperar a finalização do branch de lançamento você pode mesclar com ele também.
Finalmente, removemos o branch já mesclado:

``` 
$ git branch -d hotfix/1.2.1
Deleted branch hotfix/1.2.1 (was abbe5d6).
```

Padrão adaptado de: https://blog.ateliedocodigo.com.br/fluxo-de-versionamento-de-software-com-git-flow-b9f5195c679e